package org.bitbucket.javatek.email;

import javax.annotation.Nonnull;
import java.text.MessageFormat;

import static org.bitbucket.javatek.email.EmailPattern.PATTERN;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class Email {
  @Nonnull
  private final String value;

  public Email(@Nonnull String value) {
    this.value = requireNonEmpty(value);

    if (!PATTERN.matcher(value).matches()) {
      throw new MalformedEmailException(
        MessageFormat.format("{0} is not an email", value)
      );
    }
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    return other == this || (
      other != null
        && other.getClass() == Email.class
        && ((Email) other).value.equalsIgnoreCase(this.value)
    );
  }

  @Override
  public String toString() {
    return value;
  }
}
