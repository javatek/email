package org.bitbucket.javatek.email;

import java.util.regex.Pattern;

/**
 *
 */
interface EmailPattern {
  Pattern PATTERN = Pattern.compile(
    "^" +
      "[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*" +
      "@" +
      "(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}" +
      "$"
  );
}
