package org.bitbucket.javatek.email;

/**
 *
 */
public final class MalformedEmailException extends IllegalArgumentException {
  MalformedEmailException(String message) {
    super(message);
  }
}
