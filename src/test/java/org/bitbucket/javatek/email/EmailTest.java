package org.bitbucket.javatek.email;

import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class EmailTest {
  @Test
  public void constructGoodEmails() {
    new Email("ivan.p.nikolaev@gmail.com");
    new Email("233423@mail.ru");
    new Email("m@mail.ru");
    new Email("Sasha@mail.ru");
    new Email("SashaSergeevich@mail.ru");
    new Email("petya83@mail.ru");
    new Email("olya__petrova@mail.ru");
    new Email("M.vasechkin@bk-m.ru");
    new Email("misha.p@mail.ru");
    new Email("danya@ukr.net");
    new Email("ali-ba-ba.98@mail.ru");
    new Email("Baba.Etsu.co@gmail.com");
    new Email("xx-y@yandex.ru");
  }

  @Test
  public void constructBadEmail1() {
    assertThrows(
      MalformedEmailException.class,
      () -> new Email("misha@yandex.ru;")
    );
  }
}